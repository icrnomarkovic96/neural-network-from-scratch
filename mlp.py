import numpy as np

class ReLu():

    def forward_pass(self, h_in, eval):
        if not eval: self.h = h_in
        return np.where(h_in > 0.0, h_in, 0.0)

    def backward_pass(self):
        return np.where(self.h > 0.0, 1.0, 0.0)




class MLP_layer():

    def __init__(self, input_dim, output_dim, use_activation):
        '''
        Arguments:
        ----------
        :input_dim - (int) input dimension
        :output_dim - (int) ouput dimension
        :use_activation - (bool) if True layer will use ReLu as activation function, otherwise activation function won't be used. 
        '''
        # W: input_dim * output_dim
        self.W = np.random.random_sample((input_dim, output_dim)) / 10.0
        # b: 1 * output_dim
        self.b = np.random.random_sample((1, output_dim)) / 10.0

        self.use_activation = use_activation
        if self.use_activation:
            self.relu = ReLu()

    def forward_pass(self, h_in, eval):
        '''
        h_in (batch_size * input_dim)
        h_out (batch_size * output_dim)
        '''
        if not eval: self.h = h_in
        h_out = np.matmul(h_in, self.W) + self.b
        if self.use_activation:
            h_out = self.relu.forward_pass(h_out, eval)
        return h_out

    def backward_pass(self, output_error, lr):
        '''
        output_error (batch_size * output_dim)
        input_error (batch_size * input_dim)
        '''
        if self.use_activation:
            output_error = output_error * self.relu.backward_pass() 
        input_error = np.matmul(output_error, self.W.T)

        w_grad = np.matmul(self.h.T, output_error)
        b_grad = np.mean(output_error, axis=0, keepdims=True)

        self.W -= lr * w_grad
        self.b -= lr * b_grad

        return input_error




class MLP():
    '''
    Multi layer perceptron.
    '''
    def __init__(self, num_layers, input_dim, hidden_dim, output_dim):
        '''
        Arguments:
        ----------
        :num_layers - (int) number of layers
        :input_dim  - (int) input dimension
        :hidden_dim - (int) hidden dimension
        :output_dim - (int) output dimension
        '''
        self.num_layers = num_layers
        self.hidden_dim = hidden_dim
        self.input_dim  = input_dim
        self.output_dim  = output_dim

        self.layers = [ MLP_layer(self.input_dim, self.hidden_dim, use_activation=True) ]
        [ self.layers.append( MLP_layer(self.hidden_dim, self.hidden_dim, use_activation=True) ) for _ in range(num_layers-1) ]
        self.layers.append( MLP_layer(self.hidden_dim, self.output_dim, use_activation=False) )

    def forward(self, X, eval):
        '''
        X - data
        eval - if True NN is is evaluation mode, otherwise it is in train mode.
        '''
        h = X
        for layer in self.layers:
            h = layer.forward_pass(h, eval)
        return h

    def backward(self, loss, lr):
        '''
        loss - loss function derivative
        lr - learning rate
        '''
        for layer in reversed(self.layers):
            loss = layer.backward_pass(loss, lr)

    def __call__(self, X, eval=False):
        return self.forward(X, eval)



