import numpy as np


class NNLLossWithLogits():
    
    def softmax(self, x):
        exp_x_shifted = np.exp(x - np.max(x, axis=1, keepdims=True) )
        probs = exp_x_shifted / np.sum(exp_x_shifted, axis=1, keepdims=True)
        return probs

    def loss(self, pred, gold):
        return - np.mean( gold * np.log(self.softmax(pred)) )

    def grad(self, pred, gold):
        return pred - gold

    def __call__(self, pred, gold):
        return self.loss(pred, gold), self.grad(pred, gold)
    
