import numpy as np
import random

def generate_data(nbr_class, nbr_examples_per_class, nbr_dim, scale=1):
    '''
    Arguments
    ---------
    :nbr_class - (int) number of classes
    :nbr_examples_per_class - (int) number of examples per class
    :nbr_dim - example dimensionality

    Generates dataset (X - data, Y - labels).
    '''
    X = np.zeros((nbr_class*nbr_examples_per_class, nbr_dim))
    Y = np.zeros((nbr_class*nbr_examples_per_class, nbr_class))

    for n in range(nbr_class):
        row_start = n * nbr_examples_per_class
        row_end   = (n+1) * nbr_examples_per_class
        
        Y[row_start:row_end, n] = 1.0

        for dim in range(nbr_dim):
            random.seed(100*n+dim)
            rand_mean = random.randint(-10, 10)
            X[row_start:row_end, dim] = np.random.normal(loc=rand_mean, scale=scale, size=nbr_examples_per_class)

    p = np.random.permutation(X.shape[0])
    X = X[p]
    Y = Y[p]
    return X,Y

