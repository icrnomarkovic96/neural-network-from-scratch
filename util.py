import numpy as np


class StandardScaler():
    '''
    Standardize numpy array rows.
    '''
    def __call__(self, x):
        return (x - np.mean(x, axis=0)) / np.std(x, axis=0)